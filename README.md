# Gitlab Contract Example

At this moment it uses a manual command:

`pandoc GitLab_Contract_Example_Contributer_Info.md1 --verbose -o GitLab_Contract_Example.md --template=GitLab_Contract_Example_Contract_Info.md2`

to merge the 2 markdown files into 1, which can then be feeded in to the LaTeX Template. This should of course be incoorporated into the makefile.

Outcome should be visible [here](https://dimitrieh.gitlab.io/gitlab_example_contract/GitLab_Contract_Example.pdf).

**Run it locally with**
```
docker run -it -v `pwd`:/source strages/pandoc-docker /bin/bash
```

*Note that after the container is running you have to run the following:*

```
cp fonts/* /usr/share/fonts/
fc-cache -f -v
pandoc GitLab_Contract_Example_Contributer_Info.md1 --verbose -o GitLab_Contract_Example.md --template=GitLab_Contract_Example_Contract_Info.md2
make
```
